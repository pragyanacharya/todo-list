let userInput = (prompt('What do you want to perform?')).toLowerCase();
const list = [];

while (userInput !== 'quit') {
	if (userInput === 'new') {
		userInput = prompt('Mention the TODO List');
		list.push(userInput);
		console.log(`${userInput} added to list`);
	}
	else if (userInput === 'list') {
		console.log(`****************************************`);
		for (i = 0; i < list.length; i++) {
			console.log(`${i}: ${list[i]}`);
		}
		console.log(`*****************************************`);
	}
	else if (userInput === 'delete') {
		userInput = parseInt(prompt("Enter index no:"));
		if(!Number.isNaN(userInput)){
		list.splice(userInput, 1);
		console.log(`Todo Deleted`);
		}else{
			alert("Please enter number only");
		}
	}
	userInput = (prompt('What do you want to perform?')).toLowerCase();
}
console.log('You Quit');